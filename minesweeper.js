

const GRID_WIDTH = 16;
const GRID_HEIGHT = 16;
const BOMB_MAX = 50;
let bombsLeft = 0;
let died = false;



const BLANK = 0;
const BOMB = 1;
const BLANK_FLAG = 2;
const BOMB_FLAG = 3;
const REVEALED = 4;


const mineSweeperGridData = [];
const gridCellRefs = [];

const handleFlag = (row, col) => {
    if (died) return;

    const currentCell = gridCellRefs[row][col];
    const currentCellData = mineSweeperGridData[row][col];

    console.log(currentCellData);



    if (currentCellData === BLANK || currentCellData === BOMB) {
        currentCell.classList.remove('grid-cell-unclicked');
        currentCell.classList.add('grid-cell-flag');
        mineSweeperGridData[row][col] = (currentCellData === BOMB) ? BOMB_FLAG : BLANK_FLAG;
        bombsLeft --;
        updateBombDisplay();
        return;
    }

    if (currentCellData === BOMB_FLAG || currentCellData === BLANK_FLAG) {
        currentCell.classList.remove('grid-cell-flag');
        currentCell.classList.add('grid-cell-unclicked');
        bombsLeft ++;
        updateBombDisplay();
        mineSweeperGridData[row][col] = (currentCellData === BOMB_FLAG) ? BOMB : BLANK;
        return;
    }
}


const revealArea = (row, col, clicked, prevCount=0) => {



    if (died) {
        return;
    }

    const currentCell = gridCellRefs[row][col];
    const currentCellData = mineSweeperGridData[row][col];
    // console.log(currentCell, currentCellData);

    if (clicked) {
        currentCell.classList.remove('grid-cell-unclicked');
        if (currentCellData == BOMB) {
            currentCell.classList.add('grid-cell-bomb');
            died = true;
            updateBombDisplay();
            return;
        }
    }



    if (currentCell === BOMB_FLAG || currentCellData === BLANK_FLAG) {
        return;
    }

    currentCell.classList.add('grid-cell-blank');
    mineSweeperGridData[row][col] = REVEALED;
    
    let neightborBombCount = 0;

    for (let i = -1; i < 2; i++) {
        const neighborRow = row + i;
        // console.log(neighborRow);
        if (neighborRow >= GRID_HEIGHT || neighborRow < 0) continue;
        for (let j = -1; j < 2; j++) {
            const neightborCol = col + j;


            if (neightborCol >= GRID_WIDTH || neightborCol < 0) continue;
            if (neighborRow === row && neightborCol === col) continue;

            // console.log(neighborRow, neightborCol);
            const neighborCellData = mineSweeperGridData[neighborRow][neightborCol];
            if (neighborCellData === BOMB) {
                neightborBombCount++;
            } else if (neighborCellData === BLANK && neightborBombCount + prevCount <1 ) {
                // debugger;
                if (i === 0 || j === 0) {
                    revealArea(neighborRow, neightborCol, false, neightborBombCount);
                }


            }
        }
    }

    if (neightborBombCount > 0) {
        currentCell.innerText = neightborBombCount;
    }
}

const updateBombDisplay = () => {
    document.querySelector('.bombNumberArea').innerText = died? 'You Died' :  bombsLeft;
}


export const startMineSweeper = () => {

    const gridArea = document.querySelector('.grid-area');

    for (let i = 0; i < GRID_HEIGHT; i++) {
        const gridRow = document.createElement('div');
        gridRow.classList.add('grid-row');
        mineSweeperGridData[i] = [];
        gridCellRefs[i] = [];

        for (let j = 0; j < GRID_WIDTH; j++) {
            const gridCell = document.createElement('div');
            gridCell.classList.add('grid-cell');
            gridCell.classList.add('grid-cell-unclicked');
            gridCell.setAttribute("row", i);
            gridCell.setAttribute("col", j);
            gridRow.appendChild(gridCell);
            gridCellRefs[i][j] = gridCell;

            let isBomb = false;

            if (bombsLeft < BOMB_MAX) {
                isBomb = Math.random() < ((BOMB_MAX - bombsLeft) / ((GRID_HEIGHT * GRID_WIDTH) - (i * GRID_WIDTH + j)));
            }

            if (isBomb) {
                bombsLeft ++;
            }

            gridCell.addEventListener('click', () => {
                revealArea(i, j, true);
            });

            gridCell.addEventListener('contextmenu', (e) => {
                e.preventDefault();
                e.stopPropagation();
                handleFlag(i, j);
            });

            mineSweeperGridData[i][j] = isBomb ? BOMB : BLANK;
        }
        gridArea.appendChild(gridRow);
    }

    console.log(bombsLeft);
    updateBombDisplay(false);

    

    console.log(mineSweeperGridData);
}